import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import { HomePage} from '../home/home';
import firebase from 'firebase';

/**
 * Generated class for the ChattingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chatting',
  templateUrl: 'chatting.html',
})
export class ChattingPage {

public fireAuth : any;
user:any;



  constructor(public nav: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

  this.fireAuth = firebase.auth();
    console.log('ionViewDidLoad ChattingPage');

    this.user = this.navParams.get('user');
    //alert(JSON.stringify(this.user));
  }


openChat(){


    this.nav.push(ChatPage);
}

 logOut(){
	  this.fireAuth.signOut();
	  console.log('logged out');
	  this.nav.setRoot(HomePage);
  }

}
